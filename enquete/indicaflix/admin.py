from django.contrib import admin
from .models import Indicacoes, Pessoa

admin.site.register(Indicacoes)
admin.site.register(Pessoa)
