from django.shortcuts import render, reverse, redirect
#from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
#from django.contrib.auth.decorators import login_required
from django.views import generic
from django.utils import timezone
from .models import Indicacoes

class IndicaFlixView(generic.ListView):
    model = Indicacoes
    template_name = 'indicaflix/index.html'

    def get_queryset(self):
        return Indicacoes.objects.filter(data_indicacao__lte=timezone.now()).order_by('-data_indicacao')

class DetalhamentoView(generic.DetailView):
    model = Indicacoes
    template_name = 'indicaflix/detalhamento.html'

class InteressesView(generic.ListView):
    model = Indicacoes
    template_name = 'indicaflix/lista-interesses.html'

#class acionarInteresseView(generic.DetailView):
#    model = Indicacoes
#    template_name = 'indicaflix/acionou-interesse.html'

def acionarInteresse(request, pk):
    if request.user.is_authenticated:
        indicacao_interesse = Indicacoes.objects.get(pk=pk)
        request.user.pessoa.interesse.add(indicacao_interesse)
        return render(request, 'indicaflix/acionou-interesse.html', {'indicacao_interesse':indicacao_interesse})
    else:
        messages.info(request, 'É necessário fazer login para concluir essa ação!')
        return redirect(reverse('indicaflix:login'))

def login_view(request):
    if request.user.is_authenticated:
        return redirect(reverse('indicaflix:index'))
    else:
        if request.method == 'POST':
            usuario = request.POST.get('username')
            senha = request.POST.get('senha')
            tentativa_login = authenticate(request, username=usuario, password=senha)

            if tentativa_login is not None:
                login(request, tentativa_login)
                return redirect(reverse('indicaflix:index'))
            else:
                messages.error(request, 'Usuário não encontrado: tente novamente')

        return render(request, 'indicaflix/login.html', {})

def logout_view(request):
    logout(request)
    return redirect(reverse('indicaflix:index'))

def removerIndicacaoDoInteresse(request, pk):
    indicacao_retirada = Indicacoes.objects.get(pk=pk)
    request.user.pessoa.interesse.remove(indicacao_retirada)
    return render(request, 'indicaflix/removeu-interesse.html', {'indicacao_retirada':indicacao_retirada})







