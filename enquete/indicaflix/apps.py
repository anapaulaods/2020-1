from django.apps import AppConfig


class IndicaflixConfig(AppConfig):
    name = 'indicaflix'
