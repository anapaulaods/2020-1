from django.contrib.auth.models import User
from django.db import models

class Pessoa(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.EmailField(max_length=255)
    senha = models.CharField(max_length=30)
    nome = models.CharField(max_length=200)
    interesse = models.ManyToManyField("Indicacoes", blank=True, symmetrical=False)

class Indicacoes(models.Model):
    nome = models.CharField(max_length=200)
    descricao = models.CharField(max_length=500)
    duracao = models.CharField(max_length=200)
    data_publicacao = models.CharField(max_length=50)
    genero = models.CharField(max_length=100)
    autor = models.CharField(max_length=100)
    data_indicacao = models.DateTimeField('Data da indicação')
    tipo = models.CharField(max_length=50, null=False)









