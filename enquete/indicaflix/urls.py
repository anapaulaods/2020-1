from django.urls import path
from . import views

app_name = 'indicaflix'
urlpatterns = [
    path('',
        views.IndicaFlixView.as_view(), name='index'
    ),
    path('<int:pk>',
        views.DetalhamentoView.as_view(), name = 'detalhamento'
    ),
    path('lista-interesses',
        views.InteressesView.as_view(), name='lista-interesses'
    ),
    path('acionar-interesse/<int:pk>',
        views.acionarInteresse, name='acionar-interesse'
    ),
    path('login',
        views.login_view, name='login'
    ),
    path('logout',
        views.logout_view, name='logout'
    ),
    path('remover-interesse/<int:pk>',
        views.removerIndicacaoDoInteresse, name='remover-interesse'
    ),
    ]